# Useful Caches

## C++
### –––– Measuring Execution Time ––––
```{cpp}
// Placeholder for execution time
std::vector<double> execTime;

// Clock init
std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
std::chrono::duration<double> timeCost;

// Start clock
start = std::chrono::high_resolution_clock::now();

// Some workload to be measured goes here

// End clock 
end = std::chrono::high_resolution_clock::now();
timeCost = end - start;
execTime.push_back(std::chrono::duration<double, std::milli> (timeCost).count());
```

## Linux
### –––– Clearing caches ––––

```{shell}
free > /dev/null && sync > /dev/null && sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches' && free > /dev/null
```

### –––– SSL/TLS certificate ––––
* Initial obtain of certificate
```{shell}
sudo certbot certonly --standalone -d <Domain Name>
```

* Renew TLS certificate
```{shell}
certbot renew
```

### –––– ProxyJump ––––

**Local ⟶ Remote B ⟶ Remote C**
* `Remote B` is on same physical network as `Remote C` but on same VPN network as `Local`
* Purpose: Instead of manually ssh from `Local` ⟶ `Remote B` ⟶ `Remote C`, ProxyJump automates that process

**Edit .ssh/config in `Local` machine**
```{shell}
Host <Remote C Name>
  HostName <Remote C IP Address>
  User <Remote C Username>
  IdentityFile <Location of SSH Key>
  Port <Remote C SSH Port Number>

  # ProxyJump
  ProxyJump <Remote B Username>@<Remote B IP Address>

  # ProxyCommand
  ProxyCommand ssh -W %h:%p <Remote B IP Address>
```

**Local ⟶ Remote B ⟶ Remote C ⟶ Remote D**
* Same as previous case but this time requires additional hop to reach destination

**Edit .ssh/config in `Local` machine**
```{shell}
Host <Remote D Name>
  ProxyJump <Remote C IP Address>
  IdentityFile <Location of SSH Key>
  Hostname <Remote D IP Address>
  User <Remote D Username>
  Port <Remote D SSH Port Number>

  # Forward specific port from Remote D to Local
  LocalForward <Port Number> localhost:<Port Number>
```

**Local Forwarding**
* To access IPMI behind NAT using bastion node
```{shell}
ssh -L <Unused local port>:<Target local IP>:<Target port> -l <Bastion Username> -N <Bastion Public IP>
```

## Overleaf
### –––– Export to image ––––
1. Create a file name `latexmkrc`
2. Add this to `latexmkrc`
```{shell}
END { system('convert -density 300 -background white -flatten output.pdf pages.jpeg'); }
```
3. Once compiled, navigate to `Logs and output files`. The output image is under `Other logs and files`

### –––– Self-hosted Overleaf ––––
* Stop & Re-crate container – Run this to reflect changes 
```{shell}
bin/stop && bin/docker-compose rm -f sharelatex && bin/up
```

* Run container as a service
```{shell}
./bin/start
```

* Overleaf config file is under `/overleaf/config/overleaf.rc`
